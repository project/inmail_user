---SUMMARY---

This module creates users based on incoming email senders.


---INTRODUCTION---


This module contains an Inmail plugin that generates users from the sender of
the incoming emails.


---REQUIREMENTS---


The Inmail module.


---INSTALLATION---


Install as usual. Place the entirety of this directory in the /modules 
folder of your Drupal installation. 


---CONFIGURATION---


Set up the right behavior for you on admin/config/system/inmail_user.


---CONTACT---

Current Maintainers:
*Balogh Zoltán (zlyware) - https://www.drupal.org/u/u/zoltán-balogh
